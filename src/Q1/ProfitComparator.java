package Q1;

import java.util.Comparator;

public class ProfitComparator implements Comparator<Company> {
	public int compare(Company arg0, Company arg1) {
		if(arg0.in - arg0.out > arg1.in-arg1.out){
			return 1;
		}
		else{
			return -1;
		}
	}
}
