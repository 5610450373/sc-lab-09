package Q1;

import java.util.Comparator;

public class EarningComparator implements Comparator<Company> {
	
	@Override
	public int compare(Company arg0, Company arg1) {
		if(arg0.in > arg1.in){
			return 1;
		}
		else{
			return -1;
		}
	}

}
