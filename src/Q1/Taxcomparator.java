package Q1;

import java.util.Comparator;

public class Taxcomparator implements Comparator<Taxable> {

	@Override
	public int compare(Taxable o1, Taxable o2) {
		if(o1.getTax() > o2.getTax()){
			return 1;
		}
		else{
			return -1;
		}
	}
	

}
