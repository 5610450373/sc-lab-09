package Q1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class Testcase {
	public static void main(String[] args){
		List<Person> lis1 = new ArrayList<Person>();
		Person p1 = new Person("P1",20000);
		Person p2 = new Person("P2",10000);
		lis1.add(p1);
		lis1.add(p2);
		Collections.sort(lis1);
		System.out.println("compare method (Person)");
		if(p1.compareTo(p2) >0){
			System.out.println(p1.name+" more than "+p2.name);
		}
		else{
			System.out.println(p2.name+" more than "+p1.name);
		}
		System.out.println("Collection sort");
		for(int i=0;i<lis1.size();i++){
			System.out.println(lis1.get(i).name+" : "+lis1.get(i).salar);
		}
		System.out.println("----------------------------");
		
		List<Product> lis2 = new ArrayList<Product>();
		Product pro1 = new Product("PRO1", 300);
		Product pro2 = new Product("PRO2", 400);
		lis2.add(pro2);
		lis2.add(pro1);
		Collections.sort(lis2);
		System.out.println("compare method (product)");
		if(pro1.compareTo(pro2) >0){
			System.out.println(pro1.name+" more than "+pro2.name);
		}
		else{
			System.out.println(pro2.name+" more than "+pro1.name);
		}
		System.out.println("Collection sort");
		for(int i=0;i<lis2.size();i++){
			System.out.println(lis2.get(i).name+" : "+lis2.get(i).value);
		}
		System.out.println("----------------------------");
		
		List<Company> lis3 = new ArrayList<Company>();
		Company com1 = new Company("COM1",100000,30000);
		Company com2 = new Company("COM2",80000,50000);
		EarningComparator earn = new EarningComparator();
		ExpenseComparator expen = new ExpenseComparator();
		ProfitComparator profit = new ProfitComparator();
		
		lis3.add(com1);
		lis3.add(com2);
		System.out.println("compare method (EarningComparator)");
		if(earn.compare(com1, com2)>0){
			System.out.println(com1.com_name+" more than "+com2.com_name);
		}
		else{
			System.out.println(com2.com_name+" more than "+com1.com_name);
		}
		Collections.sort(lis3, earn);
		System.out.println("Collection sort");
		for(int i=0;i<lis3.size();i++){
			System.out.println(lis3.get(i).com_name+" : "+lis3.get(i).in);
		}
		System.out.println("----------------------------");
		
		System.out.println("compare method (ExpenseComparator)");
		if(expen.compare(com1, com2)>0){
			System.out.println(com1.com_name+" more than "+com2.com_name);
		}
		else{
			System.out.println(com2.com_name+" more than "+com1.com_name);
		}
		Collections.sort(lis3, expen);
		System.out.println("Collection sort");
		for(int i=0;i<lis3.size();i++){
			System.out.println(lis3.get(i).com_name+" : "+lis3.get(i).out);
		}
		System.out.println("----------------------------");
		
		System.out.println("compare method (ProfitComparator)");
		if(profit.compare(com1, com2)>0){
			System.out.println(com1.com_name+" more than "+com2.com_name);
		}
		else{
			System.out.println(com2.com_name+" more than "+com1.com_name);
		}
		Collections.sort(lis3, profit);
		System.out.println("Collection sort");
		for(int i=0;i<lis3.size();i++){
			System.out.println(lis3.get(i).com_name+" : "+(lis3.get(i).in-lis3.get(i).out));
		}
		System.out.println("----------------------------");
		
		List<Taxable> lis4 = new ArrayList<Taxable>();
		Taxcomparator tax = new Taxcomparator();
		lis4.add(p1);
		lis4.add(pro1);
		lis4.add(com1);
		System.out.println("Taxcomparator");
		Collections.sort(lis4, tax);
		for(int i=0;i<lis4.size();i++){
			System.out.println(lis4.get(i).getTax());
		}
		System.out.println("----------------------------");
	}
}
