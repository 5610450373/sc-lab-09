package Q1;

public class Company implements Taxable  {
	String com_name;
	double in;
	double out;
	
	public Company(String n,double i,double o){
		com_name = n;
		in = i;
		out = o;
	}
	
	public double getTax() {
		double val = in-out;
		double ans = (val*30)/100;
		return ans;
	}


}