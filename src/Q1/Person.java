package Q1;

public class Person implements Comparable<Object>,Taxable {
	String name;
	double salar;
	public Person(String n,double val){
		name = n;
		salar = val;
	}
	
	
	public int compareTo(Object otherObject) {
	 	Person other = (Person) otherObject;
		if (salar < other.salar ) { 
			return -1;
		}
		else { 
			return 1;
		}
	}

	@Override
	public double getTax() {
		double sum=0;
		if(salar>300000){
			double tax1 = (300000*5)/100;
			double tax2 = ((salar-300000)*10)/100;
			sum = tax1+tax2;
			}
		else{
			double tax = (salar*5)/100;
			sum=tax;
		}
		return sum;
	}

}