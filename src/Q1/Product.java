package Q1;

public class Product implements Comparable<Object>,Taxable {
	String name;
	double value;
	public Product(String n,double v){
		name = n;
		value = v;
		
	}
	@Override
	public int compareTo(Object otherObject) {
		Product other = (Product) otherObject;
		if (value < other.value ) { 
			return -1;
		}
		else { 
			return 1;
		}
	}
	public double getTax() {
		double ans = (value*7)/100;
		return ans;
	}
	
}