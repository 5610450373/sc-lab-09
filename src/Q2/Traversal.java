package Q2;

import java.util.ArrayList;

public interface Traversal {
	ArrayList<String> traverse(Node node);

}